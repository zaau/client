import {Routes, RouterModule} from '@angular/router';

import {NotesComponent} from './notesList/notes.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {NoteDetailComponent} from './noteDetail/note-detail.component';
import {NoteFormComponent} from './noteForm/noteForm.component';
import {LoginComponent} from './login/login.component';

import {AuthGuard} from './auth/authGuard.service';
/*
const appRoutes: Routes = [
    {path:'', redirectTo:'/notes', pathMatch:'full'},
    {path:'notes',component:NotesComponent, canActivate:[AuthGuard]},
    {path:'login',component:LoginComponent},
    {path:'notes/create',component:NoteFormComponent},
    {path:'dashboard', component:DashboardComponent, canActivate:[AuthGuard]},
    {path:'details/:id', component:NoteDetailComponent}
];
*/
const appRoutes: Routes = [
    {path:'', redirectTo:'/notes', pathMatch:'full'},
    {path:'notes', component:NotesComponent, canActivate:[AuthGuard]},
    {path:'dashboard', component:DashboardComponent},
    {path:'notes/create', component:NoteFormComponent},
    {path:'login', component:LoginComponent},
    {path:'details/:id', component:NoteDetailComponent}
];

export const routing = RouterModule.forRoot(appRoutes);