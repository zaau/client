export class NoteForm{
    constructor(
        public name: string,
        public longDescription: string,
        public tags: string,
        public shortDescription?: string
    ){}
}