export class Note{
    public _id: string;
    public name: string;
    public longDescription: string;
    public tags: string;
    public shortDescription?: string;
}