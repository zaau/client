import { Component, OnInit } from "@angular/core";
import { Note } from '../note';
import { NoteDetailComponent } from '../noteDetail/note-detail.component';
import { NoteService } from '../note.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

var markdownString = 'Aurimas\n ```js\n console.log("hello"); \n```';


@Component({
    selector: "my-notes",
    styles: [`.mdl-card{width:100%;}`],
    templateUrl: 'notes.component.html'
})
export class NotesComponent implements OnInit {

    textarea = "";
    selectedNote: Note;
    notes: Note[];
    noteText: String;
    notesS: Note[];


    constructor(private noteService: NoteService, private router: Router) { }

    ngOnInit(): void {
        this.getNotes();
        console.log("notes!")
    }

    onSearched(notesEvents: Observable<Note[]>): void {
        notesEvents
            .subscribe(notes => this.notes = notes);
    }
    delete(note: Note): void {
        this.noteService
            .delete(note._id)
            .then(() => {
                this.notes = this.notes.filter(n => n != note);
                if (this.selectedNote === note) { this.selectedNote = null; }
            });
    }
    getNotes(): void {
        this.noteService.getNotes()
            .subscribe(notes => this.notes = notes);
    }

    gotoDetail(note: Note): void {
        this.router.navigate(['/details', note._id]);
    }
}
