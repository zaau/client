import { Component, Input, OnInit } from "@angular/core";
import { ActivatedRoute, Params, Router } from '@angular/router';

import { NoteService } from './../note.service';

import { Note } from './../note';

@Component({
    selector: 'my-note-detail',
    templateUrl: 'note-detail.component.html'
})
export class NoteDetailComponent implements OnInit {
    @Input()
    note: Note;
    editNote: Boolean;
    id: string;

    constructor(private noteService: NoteService, private route: ActivatedRoute, private router: Router) { }

    ngOnInit(): void {
        this.route.params.forEach((params: Params) => {
            this.id = params['id'];
            this.noteService.getNote(this.id).then(note => this.note = note);
        })
    }

    save(): void {
        this.note._id = this.id;
        this.noteService.update(this.note).then(this.goBack);
    }
    delete(note: Note): void {
        this.noteService
            .delete(note._id)
            .then(() => {
                this.router.navigate(['/notes']);
            });
    }
    goBack(): void {
        window.history.back();
    }
    edit(): void {
        this.editNote = true;
    }
    cancel(): void {
        this.editNote = false;
    }

}