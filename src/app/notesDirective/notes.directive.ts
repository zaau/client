import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Note } from '../note';

@Component({
    selector: 'notesList',
    styles: [`.mdl-card{width:100%;}`],
    templateUrl: 'notes.directive.html'
})
export class NotesDirective {
    @Input() notes: string;
    constructor(private router: Router) { }

    gotoDetail(note: Note) {
        this.router.navigate([`/details/${note._id}`]);
    }

}