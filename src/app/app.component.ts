import { Component } from '@angular/core';
import {Headers, Http} from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-root',
  templateUrl:'app.component.html'
})
export class AppComponent {
  title = 'app worksss!';
  private headers = new Headers({'Content-type': 'application/json'});
  constructor(private http: Http) {}
  model={}
    interval= {
      dayCycle : [0,0],
      startDate:0,
      endDate:0
    };
    

    add(data: any ): void {
    this.http
      .post('http://localhost:3000/addInterval', JSON.stringify({data: data}), {headers: this.headers})
      .toPromise()
      .then(res => res.json);
  }
}
