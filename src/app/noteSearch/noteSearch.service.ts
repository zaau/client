import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { Note } from '../note';

@Injectable()
export class NoteSearchService {
    notesUrl = `${environment.serverPath}/notes/`;
    constructor(private http: Http) { }



    search(name: string): Observable<Note[]> {
        return this.http
            .get(this.notesUrl + "/search/" + name)
            .map((r: Response) => this.extractData(r));
    }
    handleError(error: any) {
        let errMsg = (error.message) ? error.message
            : error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);

    }
    private extractData(res: Response) {
        let body = res.json();
        return body as Note[];
    }
}