import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { NoteSearchService } from './noteSearch.service';
import { NoteService } from '../note.service'
import { Note } from '../note';

@Component({
    selector: 'note-search',
    templateUrl: 'noteSearch.component.html',
    providers: [NoteSearchService, NoteService],
    styles: [`#search-component{height:100%;}`]
})
export class NoteSearchComponent implements OnInit {

    notes: Observable<Note[]>;
    private searchTerms = new Subject<string>();

    @Output() onSearched = new EventEmitter<Observable<Note[]>>();
    searched = false;

    constructor(private noteSearchService: NoteSearchService, private router: Router, private noteService: NoteService) { }

    search(term: string): void {
        if (term.length < 1) {
            this.onSearched.emit(this.noteService.getNotes());
        }
        this.searchTerms.next(term);
    }

    ngOnInit(): void {
        this.notes = this.searchTerms
            .debounceTime(300)
            .distinctUntilChanged()
            .switchMap(term => {
                if (term) {
                    var searchResults = this.noteSearchService.search(term);
                    console.log("from search: ", searchResults);
                    this.onSearched.emit(searchResults);
                    return searchResults;
                } else {
                    return Observable.of<Note[]>([])
                }
            })
            .catch(error => {
                console.log(error);
                return Observable.of<Note[]>([]);
            });
    }
}