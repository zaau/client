import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { LoginData } from './loginModel';
import { AuthService } from '../auth/auth.service';

@Component({
    selector: 'login',
    templateUrl: 'login.component.html'
})
export class LoginComponent {
    model = new LoginData("", "");
    constructor(private authService: AuthService, private router: Router) { }

    onSubmit(username: string, password: string) {
        this.authService.login(this.model)
            .subscribe((result) => {
                result && 
                    this.router.navigate(['/']);
            });
    }
}