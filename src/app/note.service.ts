import {Injectable} from '@angular/core';
import {Note} from './note';
import {Headers, Http} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {Observable} from 'rxjs';
import {environment} from '../environments/environment';
import {NoteForm} from './noteForm';

import { AuthHttp } from 'angular2-jwt';

@Injectable()
export class NoteService{

    private notesUrl = `${environment.serverPath}/notes`;
    private headers = new Headers({'Content-type':'application/json'});

    constructor(private http: Http, private authHttp:AuthHttp){}

    update(note:Note):Promise<Note>{
        const url = `${this.notesUrl}/${note._id}`;
        return this.http
            .put(url, JSON.stringify(note), {headers:this.headers})
            .toPromise()
            .then(() => note)
            .catch( this.handleError);
    }
    create(newNote:NoteForm):Promise<any>{
        console.log(newNote);
        return this.http
            .post(this.notesUrl, JSON.stringify(newNote), {headers: this.headers})
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    delete(id:string):Promise<void>{
        let url = `${this.notesUrl}/${id}`;
        return this.http.delete(url, {headers:this.headers})
            .toPromise()
            .then(()=>null)
            .catch(this.handleError);

    }
    getNotes():Observable<Note[]>{
        return this.authHttp.get(this.notesUrl)
            .map( response => response.json() as Note[]);
    }
    getNote(id: string): Promise<Note> {
        const url = `${this.notesUrl}/${id}`;
        /*return this.getNotes()
            .then(notes => notes.find(note => note._id === id));*/
        return this.http.get(url,{headers:this.headers})
            .toPromise()
            .then(response => response.json() as Note)
            .catch(this.handleError);    
    }
    handleError(error:any):Promise<any>{

        console.error('An error occoured', error);

        return Promise.reject(error.message || error);

    }
}