import { Component, OnInit } from '@angular/core';
import { Note } from './../note';
import { NoteService } from './../note.service';
import { Router } from '@angular/router';

@Component({
    selector: 'my-dashboard',
    templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {
    notes: Note[] = [];

    constructor(private noteService: NoteService, private router: Router) { }

    ngOnInit(): void {
        this.noteService.getNotes().subscribe(notes => this.notes = notes);
    }

    gotoDetail(note: Note): void {
        let link = ['/details', note._id];
        this.router.navigate(link);
    }
}