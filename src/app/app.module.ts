import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MdlModule } from '@angular-mdl/core';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { routing } from './app.routing';

import {NotesComponent} from './notesList/notes.component';
import {NoteDetailComponent} from './noteDetail/note-detail.component'
import {DashboardComponent} from './dashboard/dashboard.component';
import {NoteSearchComponent} from './noteSearch/noteSearch.component';
import {NotesDirective}         from './notesDirective/notes.directive'; 
import {NoteFormComponent} from './noteForm/noteForm.component';
import {LoginComponent}         from './login/login.component';

import {NoteService} from './note.service';
import {AuthService}            from './auth/auth.service';
import {AuthGuard}              from './auth/authGuard.service';

import { provideAuth } from 'angular2-jwt';

@NgModule({
    imports: [BrowserModule, FormsModule, HttpModule, routing, MdlModule],
    declarations: [
        AppComponent,
        NotesComponent,
        DashboardComponent,
        NoteSearchComponent,
        NotesDirective,
        NoteFormComponent,
        NoteDetailComponent,
        LoginComponent],
    providers: [NoteService, AuthService, AuthGuard, provideAuth({
      tokenGetter: () => { 
          console.log(localStorage.getItem('auth_token'));
          return localStorage.getItem('auth_token') ;
        }
    })],
    bootstrap: [AppComponent]
})
export class AppModule { }

