import { Component, Input } from '@angular/core';
import { NoteForm } from './../noteForm';
import { NoteService } from '../note.service';
import { Note } from '../note';

@Component({
    selector: 'note-form',
    templateUrl: './noteForm.component.html',
    styles: [".mdl-textfield{width:100%;}"]
})
export class NoteFormComponent {
    active = true;
    success = false;
    private model: NoteForm = new NoteForm('', '', '', '');
    private noteModel: Note;
    private _editNote: Boolean = false;

    @Input()
    set editNote(editNote: Boolean) { this._editNote = editNote; }
    get editNote(): Boolean { return this._editNote; }

    @Input()
    set note(note: Note) {
        console.log("set: ", note);
        this.model = note;
        this.noteModel = note;
    }
    get note(): Note {
        console.log("get");
        return this.noteModel;
    }
    constructor(private noteService: NoteService) { }

    onSubmit() {
        if (this._editNote) {
            console.log("put oldNote: ", this.noteModel);
            this.noteService.update(this.noteModel).then(note => { this.success = true; })
        } else {
            console.log("post: ", this.model);
            debugger;
            this.noteService.create(this.model)
                .then(note => {
                    this.success = true;
                })
        }
    }
}