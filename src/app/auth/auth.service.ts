import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { LoginData } from '../login/loginModel';
import { environment } from '../../environments/environment';
import { tokenNotExpired } from 'angular2-jwt';

@Injectable()
export class AuthService {
    private loggedIn = false;
    private headers = new Headers({ 'Content-type': 'application/json' });
    private url = `${environment.serverPath}/api/auth/signin`;

    constructor(private http: Http) {
        this.loggedIn = !!localStorage.getItem("auth_token");
    }

    login(data: LoginData) {

        return this.http.post(this.url, JSON.stringify(data), { headers: this.headers })
            .map((res) => {
                                // login successful if there's a jwt token in the response
                                var response = res.json();
                                if (response.token) {
                                        // set token property
                                        this.loggedIn = true;
                                        // store username and jwt token in local storage to keep user logged in between page refreshes
                                        localStorage.setItem('auth_token', response.token);
                                        // return true to indicate successful login
                                        return true;
                                } else {
                                        // return false to indicate failed login
                                        return false;
                                }
                        });

    }
    logout() {
        localStorage.removeItem("auth_token");
        this.loggedIn = false;
    }
    userLoggedIn() {
        return tokenNotExpired("auth_token");
    }
    isLoggedIn() {
        return !!localStorage.getItem("auth_token");
    }
}